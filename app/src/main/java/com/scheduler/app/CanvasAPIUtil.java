/*
 *
 *
 * @author  Billy Lazaro.
 * @version February 29, 2020
 *
 */
package com.scheduler.app;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CanvasAPIUtil
{
    private String token;
    private String urlRoot;

    public CanvasAPIUtil(String filename) {
        try{
            BufferedReader br = new BufferedReader(new FileReader(filename));
            while (br.ready()){
                String[] temp = br.readLine().split("=");
                if (temp[0].equals("token")) {
                    this.token = temp[1];
                }else if (temp[0].equals("urlRoot")) {
                    this.urlRoot = temp[1];
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found!");
        } catch (IOException e) {
            System.out.println("Error reading File!");
        }
    }

    public String getToken() {
        return token;
    }

    public String getUrlRoot() {
        return urlRoot;
    }
}
