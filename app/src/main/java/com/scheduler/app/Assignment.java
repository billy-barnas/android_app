/*
 * Assignment class for Alexa Skill Scheduler project.
 *
 * @author  Billy Lazaro.
 * @version February 20, 2020
 *
 */
package com.scheduler.app;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.Serializable;

public class Assignment implements Comparable<Assignment>, Serializable {
    private Integer assignment_id;
    private String assignment_name;
    private String course_name;
    private DateTime deadline;
    private boolean finished;

    /**
     * Constructor for Assignment.
     *
     * @param name name of assignment.
     * @param //dl deadline of the assignment.
     */
    public Assignment(Integer id, String name, DateTime dl) {
        assignment_id = id;
        assignment_name = name.toLowerCase();
        this.deadline = dl;
        finished = false;
    }

    public Assignment(Integer id, String name) {
        assignment_id = id;
        assignment_name = name.toLowerCase();
        finished = false;
    }

    public Assignment(String name) {
        assignment_name = name.toLowerCase();
        finished = false;
    }

    /**
     * Compares deadlines.
     *
     * @param other Assignment to compare it to
     * @return
     */
    @Override
    public int compareTo(Assignment other) {
        if (getDeadline() == null || other.getDeadline() == null)
            return -1;
        return getDeadline().compareTo(other.getDeadline());
    }

    @Override
    public String toString() {
        DateTimeFormatter dtfOut = DateTimeFormat.forPattern("MM/dd/yyyy");
        return "" + assignment_name + " is due on " + dtfOut.print(this.getDeadline()) + ". ";
    }

    public boolean equals(String name) {
        return name.equals(assignment_name);
    }

    public Integer getAssignment_id() {
        return assignment_id;
    }

    public void setAssignment_id(Integer assignment_id) {
        this.assignment_id = assignment_id;
    }

    public void setAssignment_name(String assignment_name) {
        this.assignment_name = assignment_name;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public void setFinished() {
        this.finished = true;
    }

    public String getAssignment_name() {
        return assignment_name;
    }

    public DateTime getDeadline() {
        return deadline;
    }

    public boolean isFinished() {
        return finished;
    }
}
