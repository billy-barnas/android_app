package com.scheduler.app;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.scheduler.app.ui.RemindBroadcast;

import org.joda.time.DateTime;

import java.util.Calendar;

import static com.scheduler.app.R.string.reminder_saved;
import static java.lang.Integer.parseInt;

public class addAssignment extends AppCompatActivity implements TimePickerDialog.OnTimeSetListener {
    private static final long milMinute = 60000L;
    private static final long milHour = 3600000L;
    public String an;

    public int hour;
    public int minuteOfDay;
    public int year, month, day;
    Pinboard pinboard;
    String dialog_message = "";
    String dialog_title = "";
    public Calendar calendar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_assignment);


        pinboard = (Pinboard) getIntent().getSerializableExtra("pinboard");

        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
        Button timeButton = (Button) findViewById(R.id.time_picker);
        final TextView assignmentName = (TextView) findViewById(R.id.assignmentName);
        final TextView deadline = (TextView) findViewById(R.id.deadline);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 an = (String) assignmentName.getText().toString();
                String dl = (String) deadline.getText().toString();
                AlertDialog.Builder builder;
                if (an.matches("") || dl.matches("")) {
                    Log.e("addAssignment", "Input error");
                    dialog_message = "Assignment name or deadline cannot be empty!";
                    dialog_title = "Incorrect Input";
                    builder = new AlertDialog.Builder(addAssignment.this);
                    builder.setMessage(dialog_message)
                            .setTitle(dialog_title);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                } else {

                    Log.e("onClick: ", "Adding");
                    String[] a = dl.split("([-])");
                    month = parseInt(a[0]);
                    day = parseInt(a[1]);
                    year = parseInt(a[2]);
                    DateTime date = new DateTime(year, month, day, 0, 0, 0);
                    pinboard.addAssignment(an, date);

                    //checking if the user wants to create a reminder
                    if(!"".equals(hour) && !"".equals(minuteOfDay)) {
                        RemindBroadcast remindBroadcast = null;
                        createNotificationChannel();
                        intent();
                        Toast.makeText(addAssignment.this, getString(reminder_saved),
                                Toast.LENGTH_SHORT).show();
                    }
                    Log.d("hour", String.valueOf(hour));
                    Log.d("minute", String.valueOf(minuteOfDay));
                    Log.e("addAssignment", "Assignment added");
                    Log.e("addAssignment", pinboard.toString());
                    Intent intent = new Intent(addAssignment.this, MainActivity.class);
                    intent.putExtra("updatedPinboard", pinboard);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });


        timeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment timePicker = new TimePicker();
                timePicker.show(getSupportFragmentManager(), "time picker");

            }
        });

    }

    @Override
    public void onTimeSet(android.widget.TimePicker view, int hourOfDay, int minute) {
        hour = hourOfDay;
        minuteOfDay = minute;
    }


    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("Reminder", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void intent(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day, hour, minuteOfDay, 0);
        Long startTime = calendar.getTimeInMillis();
        Log.d("selected1", String.valueOf(startTime));

        long selectedTimestamp =  calendar.getTimeInMillis();
        Log.d("selected", String.valueOf(selectedTimestamp));

        Intent intent;
        intent = new Intent(addAssignment.this, RemindBroadcast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(addAssignment.this, 0, intent, 0);

        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);

        long timeOfTheButton = System.currentTimeMillis();
        long seconds = 1000 * 60; // 1000 * ((day * 86400) + (hour * 3600) + (minuteOfDay * 60));

        alarm.set(AlarmManager.RTC_WAKEUP, timeOfTheButton+seconds, pendingIntent);

    }

}
