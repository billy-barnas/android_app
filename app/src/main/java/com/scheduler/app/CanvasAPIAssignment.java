/*
 * CanvasAPI.
 *
 * @author  Billy Lazaro.
 * @version February 29, 2020
 *
 */
package com.scheduler.app;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;


public class CanvasAPIAssignment {
    private String dataFile = "src/settings.ini";
    private String token;
    private String urlRoot;

    public CanvasAPIAssignment() {
        prepare();
    }

    public void prepare() {
        CanvasAPIUtil util = new CanvasAPIUtil(dataFile);
        token = util.getToken();
        urlRoot = util.getUrlRoot();
        token = "9235~pxSxTsme3fFMln7yQK0o9Wx9KlzpV5HPTKMz06olXduuM60aXl6wksMkzBOUbWoS";
        urlRoot = "https://hsutx.instructure.com";
    }

    public String connectToCanvas(Integer id, Pinboard pinboard) {
        String assignments = "";
        try {
            String fullUrl = urlRoot + "/api/v1/courses/" + id + "/assignments?per_page=100&access_token=" + token;
            URL url = new URL(fullUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String jsonText = "";
            while (br.ready()) {
                String line = br.readLine();
                jsonText += line;
            }
            conn.disconnect();
            jsonText = "{ \"assignments\":" + jsonText + "}";
            parseJSONAssignment(jsonText, pinboard);
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return assignments;
    }

    public void parseJSONAssignment(String jsonText, Pinboard pinboard) throws JSONException {
        JSONObject obj = new JSONObject(jsonText);
        //System.out.println(obj.toString(3));
        JSONArray a = obj.getJSONArray("assignments");
        String assignments = "";
        for(int i = 0; i<a.length(); i++) {
            JSONObject temp = (JSONObject) a.get(i);
            DateTime dateTime, homeworkDate;
            Boolean pastHW = false;
            if (!temp.isNull("due_at")) {
                homeworkDate = new DateTime(temp.get("due_at"));
                pastHW = homeworkDate.isAfterNow();
            }
            Assignment assign;

            Boolean hasSubmit = (Boolean) temp.get("has_submitted_submissions");
            if (!hasSubmit && pastHW) {
                // If we allow due_at to be null then uncomment this and comment out pastHW and if statement up top.
                /*if (temp.isNull("due_at")) {
                    assign = new Assignment((Integer) temp.get("id"), (String) temp.get("name"));
                    pinboard.addAssignment(assign);
                    assignments += assign.toString();
                } else {*/
                    dateTime = new DateTime(temp.get("due_at"));
                    assign = new Assignment((Integer) temp.get("id"), (String) temp.get("name"), dateTime);
                    pinboard.addAssignment(assign);
                    assignments += assign.toString();
                //}
                assignments = assignments + "\n";
            }
        }
    }
}
