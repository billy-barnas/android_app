package com.scheduler.app;/*
 *
 *
 * @author  Billy Lazaro.
 * @version February 20, 2020
 *
 */
import android.util.Log;

import org.joda.time.DateTime;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class Pinboard implements Serializable {
    private ArrayList<Assignment> ToDo;
    private int numAssignment;
    private int idNumber;

    /**
     * Constructor method for Pinboard.
     */
    public Pinboard() {
        ToDo = new ArrayList<>();
        numAssignment = 0;
    }

    /**
     * Method to add assignment into the user's ToDo list.
     *
     * @param name Name of the assignment.
     * @param dl Deadline of the assignment.
     */
    public void addAssignment(String name, DateTime dl) {
        Assignment newAssignment = new Assignment(idNumber, name, dl);
        newAssignment.setAssignment_id(idNumber);
        ToDo.add(newAssignment);
        idNumber++;
        numAssignment++;
        System.out.println("Assignment added.");
    }

    /**
     * Method to add assignment without a due date into the user's ToDo list.
     *
     * @param name Name of the assignment.
     */
    public void addAssignment(String name) {
        Assignment newAssignment = new Assignment(name);
        ToDo.add(newAssignment);
        numAssignment++;
        System.out.println("Assignment added.");
    }

    /**
     * Method to add assignment without a due date into the user's ToDo list.
     *
     * @param newAssignment Name of the assignment.
     */
    public void addAssignment(Assignment newAssignment) {
        ToDo.add(newAssignment);
        numAssignment++;
        //System.out.println("Assignment added.");
    }

    /**
     * Searches the ToDo list for the assignment name.
     *
     * @param assignmentName Name of assignment.
     * @return index of the assignment.
     */
    public int searchAssigntment(String assignmentName) {
        for (int i = 0; i<ToDo.size(); i++) {
            if (ToDo.get(i).getAssignment_name().equals(assignmentName)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Mark assignment as finished so it can be deleted.
     *
     * @param name Name of assignment.
     */
    public void markFinished(String name) {
        int index = searchAssigntment(name);
        if (index >= 0){
            ToDo.get(index).setFinished();
        }
    }

    public boolean isFinished(String name) {
        Boolean isFinished = false;
        int index = searchAssigntment(name);
        if (index >= 0){
            ToDo.get(index).isFinished();
        }

        return false;
    }

    /**
     * Deletes the assignment if it is marked as finished.
     *
     * @param name Name of assignment.
     */
    public String deleteAssignment(String name) {
        if (searchAssigntment(name) >= 0) {
            ToDo.remove(searchAssigntment(name));
            System.out.println(String.format("Assignment %s deleted successfully", name));
            Log.e("deleteAssignment: ", "delete");
            return String.format("Assignment %s deleted successfully", name);
        }else {
            System.out.println("Assignment not found.");
            return "Assignment not found.";
        }
    }

    /**
     * Print assignments as a StringBuilder
     */
    public StringBuilder printAssignments() {
        StringBuilder assignments = new StringBuilder();
        for (Assignment d : ToDo) {
            assignments.append(d);
            assignments.append("\n");
        }
        return assignments;
    }

    /**
     * A toString method that utilizes String Builder.
     *
     * @return the assignment or "you don't have any assignments."
     */
    public String toString() {
        if (ToDo.size() == 0 ) {
            return "You don't have any assignments";
        }
        StringBuilder sb = printAssignments();
        return sb.toString();
    }

    /**
     * Sorts the ToDo array list.
     */
    public void sort() {
        Collections.sort(ToDo);
    }
}
