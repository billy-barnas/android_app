package com.scheduler.app;

public class Course {
    private String course_code;
    private String course_name;
    private Integer course_id;

    public Course(String name, String code, Integer id) {
        this.course_name = name;
        this.course_code = code;
        this.course_id = id;
    }

    public String getCourse_code() {
        return course_code;
    }

    public String getCourse_name() {
        return course_name;
    }

    public Integer getCourse_id() {
        return course_id;
    }
}
