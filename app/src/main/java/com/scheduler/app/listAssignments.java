package com.scheduler.app;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.os.Bundle;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import org.json.JSONException;

import java.io.IOException;
import java.io.Serializable;

public class listAssignments extends AppCompatActivity {
    Pinboard pinboard;
    String str = null;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_assignments);
        TextView assignment = (TextView) findViewById(R.id.assignments);
        pinboard = (Pinboard) getIntent().getSerializableExtra("pinboard");
        Log.e("onList: ", pinboard.toString());
        pinboard.sort();
        str = pinboard.toString();
        Log.e("test", pinboard.toString());
        assignment.setText(pinboard.toString());
    }
}
