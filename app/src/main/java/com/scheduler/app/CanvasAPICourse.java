/*
 * CanvasAPI.
 *
 * @author  Billy Lazaro.
 * @version February 29, 2020
 *
 */
package com.scheduler.app;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

public class CanvasAPICourse extends AsyncTask<Void, Void, Void> {
    private String dataFile = "src/settings.ini";
    private String token;
    private String urlRoot;
    private String current_term;
    private ArrayList<Course> courses;
    private String jsonText;

    public CanvasAPICourse(String current_term) throws IOException {
        this.current_term = current_term;
    }

    /**
     * Connects to CanvasAPI to get the JSON text for classes.
     * @param current_term code for the current term. Format should be 2 digits of year and FA or SP (Fall or Spring).
     * @throws IOException
     */
    public void connectToCanvas(String current_term) throws IOException {
        /*try {

            //readProfile();
            CanvasAPIUtil util = new CanvasAPIUtil(dataFile);
            token = util.getToken();
            urlRoot = util.getUrlRoot();
            token = "9235~pxSxTsme3fFMln7yQK0o9Wx9KlzpV5HPTKMz06olXduuM60aXl6wksMkzBOUbWoS";
            urlRoot = "https://hsutx.instructure.com";
            //System.out.println(token);

            String fullUrl = urlRoot + "/api/v1/users/2919/courses?per_page=100&access_token=" + token;
            URL url = new URL(fullUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            String jsonText = "";
            while (br.ready()) {
                String line = br.readLine();
                jsonText += line;
            }
            conn.disconnect();
            //System.out.println(jsonText);
            jsonText = "{ \"courses\":" + jsonText + "}";
            parseJSONCourse(current_term, jsonText);

        }catch (IOException | JSONException e){
            System.out.println("ConnectToCanvas caught IOException.");
        }*/
    }

    /**
     * Parses through the JSON text to get the courses in the current term.
     * @param current_term code for the current term. Format should be 2 digits of year and FA or SP (Fall or Spring).
     * @param jsonText JSON text from the API.
     */
    public void parseJSONCourse(String current_term, String jsonText) throws JSONException {
        JSONObject obj = new JSONObject(jsonText);
        Log.e("json", obj.toString(3));
        JSONArray a = obj.getJSONArray("courses");
        courses = new ArrayList<Course>();
        for(int i = 0; i<a.length(); i++) {
            JSONObject temp = (JSONObject) a.get(i);
            String term = (String) temp.get("name");
            term = term.substring(0, 4);
            //System.out.println(term);
            if (term.equals(current_term)) {
                Course assign = new Course((String) temp.get("name"), (String) temp.get("course_code"), (Integer) temp.get("id"));
                courses.add(assign);
                //System.out.println(courses.get(i).getCourse_name());
            }
        }
    }

    public String pullAssignment(Pinboard pinboard) {
        CanvasAPIAssignment courseAssignment = new CanvasAPIAssignment();
        Iterator<Course> iter = courses.iterator();
        String assignments = "";
        while (iter.hasNext()) {
            Integer id = iter.next().getCourse_id();
            String assignment = courseAssignment.connectToCanvas(id, pinboard);
            //pinboard.addAssignment(assignment);
        }
        return assignments;
    }

    /**
     * Displays the courses in the current term.
     */
    public void displayCourses() {
        Iterator<Course> iter = courses.iterator();
        System.out.println("Here are your courses:");
        while (iter.hasNext()) {
            System.out.println(toString(iter.next()));
        }
    }

    /**
     * toString for courses.
     * @param course Course to be printed out.
     * @return Course Name, Course Code, and Course ID.
     */
    private String toString(Course course) {
        return "Course Name: " + course.getCourse_name() + ", Course Code " + course.getCourse_code() + ", Course ID: " + course.getCourse_id();
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            //CanvasAPIUtil util = new CanvasAPIUtil(dataFile);
            //token = util.getToken();
            //urlRoot = util.getUrlRoot();
            token = "9235~pxSxTsme3fFMln7yQK0o9Wx9KlzpV5HPTKMz06olXduuM60aXl6wksMkzBOUbWoS";
            urlRoot = "https://hsutx.instructure.com";
            String fullUrl = urlRoot + "/api/v1/users/2919/courses?per_page=100&access_token=" + token;
            Log.e("full url: ", fullUrl);
            URL url = new URL(fullUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            InputStreamReader in = new InputStreamReader(conn.getInputStream());
            BufferedReader br = new BufferedReader(in);
            jsonText = "";
            Log.e("doInBackground: ", "test");
            while (br.ready()) {
                String line = br.readLine();
                jsonText += line;
                Log.e("doInBackground: ", jsonText);
            }
            conn.disconnect();
            jsonText = "{ \"courses\":" + jsonText + "}";
            Log.e("json: ", jsonText);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void onPostExecute(Void... voids) throws JSONException {
        Log.e("onPostExecute: ", jsonText);
        parseJSONCourse(current_term, jsonText);
    }
}
