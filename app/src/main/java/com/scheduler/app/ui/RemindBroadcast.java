package com.scheduler.app.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.scheduler.app.R;

public class RemindBroadcast extends BroadcastReceiver {

    public String reminderText;
    @Override
    public void onReceive(Context context, Intent intent) {
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "Reminder")
                .setSmallIcon(R.drawable.ic_add_alert_black_24dp)
                .setContentTitle("This a reminder")
                .setContentText("You have a task due")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(false);

        final NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(100, builder.build());
    }


}
