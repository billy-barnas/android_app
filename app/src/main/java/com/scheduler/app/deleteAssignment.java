package com.scheduler.app;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class deleteAssignment extends AppCompatActivity {
    Pinboard pinboard;
    String success;
    String dialog_message;
    String dialog_title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_assignment);

        pinboard = (Pinboard) getIntent().getSerializableExtra("pinboard");
        Log.e("onDelete: ", pinboard.toString());
        Button btnSubmit = (Button) findViewById(R.id.btnSubmit);
        final TextView assignmentName = (TextView) findViewById(R.id.assignmentName);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("deleteAssignment: ", "Button Clicked");
                final String an = (String) assignmentName.getText().toString();
                AlertDialog.Builder builder;
                AlertDialog dialog = null;

                if (an.matches("")) {
                    Log.e("addAssignment", "Input error");
                    dialog_message = "Assignment name cannot be empty!";
                    dialog_title = "Incorrect Input";
                    builder = new AlertDialog.Builder(deleteAssignment.this);
                    builder.setMessage(dialog_message)
                            .setTitle(dialog_title);
                    dialog = builder.create();
                    dialog.show();
                } else {
                    if (pinboard.searchAssigntment(an) >= 0) {
                        Log.e("onClick: ", "Assignment Found");
                        if (pinboard.isFinished(an)) {
                            Log.e("onClick: ", "Assignment is finished");
                            success = pinboard.deleteAssignment(an);
                            Log.e("test", "Assignment deleted");
                            Log.e("test", pinboard.toString());
                        } else {
                            Log.e("onClick: ", "Assignment is not finished");
                            String dialog_message = "This Assignment is not Finished. Are you sure you want to delete?";
                            String dialog_title = "Are You Sure?";
                            builder = new AlertDialog.Builder(deleteAssignment.this);
                            builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Log.e("onClick: ", "User clicked ok");
                                    pinboard.deleteAssignment(an);
                                    Log.e("test", "Assignment deleted");
                                    Log.e("test", pinboard.toString());
                                    dialog.dismiss();
                                    Intent intent = new Intent(deleteAssignment.this, MainActivity.class);
                                    intent.putExtra("updatedPinboard", pinboard);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            });
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                            builder.setMessage(dialog_message)
                                    .setTitle(dialog_title);

                            dialog = builder.create();
                            dialog.show();
                        }
                    } else {
                        Log.e("deleteAssignment", "Assignment not found.");
                        dialog_message = "Assignment not found. Please enter the assignment name (case sensitive).";
                        dialog_title = "Assignment not Found!";
                        builder = new AlertDialog.Builder(deleteAssignment.this);

                        builder.setMessage(dialog_message)
                                .setTitle(dialog_title);

                        dialog = builder.create();
                        dialog.show();
                    }
                }
            }
        });

    }
}
